<nav class="navbar">
    <ul>
        <li class="nav-btn">
            <a href="{{ route('posts.index') }}">Posts</a>
        </li>
        <li class="nav-btn">
            <a href="{{ route('categories.index') }}">Categories</a>
        </li>
    </ul>
</nav>
