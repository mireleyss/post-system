<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Posts system</title>
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <form method="post" action="{{ route('categories.store') }}" class="form">
                @csrf

                <div class="previous-page">
                    <a href="{{ route('categories.index') }}" class="link">Go Back</a>
                </div>
                <h1 class="form-name">Add category</h1>
                <label for="name">Category name</label>
                <input type="text" value="{{ old('name') }}" placeholder="Category name" id="name" name="name" class="form_input-text">
                @if ($errors->has('name'))
                    <span>{{ $errors->first('name') }}</span>
                @endif

                <input type="submit" class="submit-btn">
            </form>
        </div>
    </div>
</body>
