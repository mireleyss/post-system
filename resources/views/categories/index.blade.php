<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Posts system</title>
</head>
<body>
    <div class="container">
        <div class="wrapper">
            @include('/components/header')
            <div class="actions">
                <div class="add-btn">
                    <a href="{{ route('categories.create') }}">Add category</a>
                </div>
            </div>

            <h1>Categories</h1>
            <div class="data-table">
                <table>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th colspan="2">Actions</th>
                    </tr>
                    </thead>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td><a href="{{ route('posts.index', ['category_id' => $category->id]) }}">{{ $category->name }}</a></td>
                            <td>
                                <div class="edit-btn">
                                    <a href="{{route('categories.edit', $category)}}">Edit</a>
                                </div>
                            </td>
                            <td>
                                <form action="{{ route('categories.destroy', $category) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" onclick="return confirm('Are you sure you want to delete this category?')" class="delete-btn">
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</body>
