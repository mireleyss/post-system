<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Posts system</title>
</head>
<body>
    <div class="container">
        <div class="wrapper">
            <form method="post" action="{{ route('posts.update', $post) }}" class="form">
                @csrf
                @method('PUT')

                <div class="previous-page">
                    <a href="{{ route('posts.index') }}" class="link">Go Back</a>
                </div>

                <h1 class="form-name">Edit post</h1>

                <label for="header">Header</label>
                <input type="text" value="{{ old('header', $post->header) }}" placeholder="Header" id="header" name="header" class="form_input-text">
                @if ($errors->has('header'))
                    <span>{{ $errors->first('header') }}</span>
                @endif

                <label for="content">Content</label>
                <textarea id="content" name="content" class="form_input-textarea">{{ old('content', $post->content) }}</textarea>
                @if ($errors->has('content'))
                    <span>{{ $errors->first('content') }}</span>
                @endif

                <label for="category_id">Category</label>
                <select id="category_id" name="category_id" class="form_input-option">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" @selected($post->category->id == $category->id)>{{$category->name}}</option>
                    @endforeach
                </select>

                <input type="submit" class="submit-btn">
            </form>
        </div>
    </div>
</body>
