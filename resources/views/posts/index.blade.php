<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <title>Posts system</title>
</head>
<body>
    <div class="container">
        <div class="wrapper">
            @include('/components/header')

            <div class="actions">
                <div class="add-btn">
                    <a href="{{ route('posts.create') }}">Add post</a>
                </div>
                <form method="get" action="{{ route('posts.index') }}">
                    @csrf
                    <select id="category_id" name="category_id" class="dropdown-menu">
                        <option selected="true" disabled>--Select category--</option>
                        @foreach($categories as $category)
                            <a href="{{ route('posts.filter', $category) }}"><option value="{{$category->id}}" @selected(old('category_id') == $category->id)>{{$category->name}}</option></a>
                        @endforeach
                    </select>

                    <input type="submit" class="submit-btn">
                </form>
            </div>

            <h1>Posts</h1>
            <div class="data-table">
                <table>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Header</th>
                        <th>Category</th>
                        <th colspan="2">Actions</th>
                    </tr>
                    </thead>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ Str::limit($post->header, 50) }}</td>
                            <td>
                                <a href="{{ route('posts.index', ['category_id' => $post->category->id]) }}">
                                    {{ $post->category->name }}
                                </a>
                            </td>
                            <td>
                                <div class="edit-btn">
                                    <a href="{{route('posts.edit', $post)}}" >Edit</a>
                                </div>
                            </td>
                            <td>
                                <form action="{{ route('posts.destroy', $post) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" onclick="return confirm('Are you sure you want to delete this post?')" class="delete-btn">
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            {{ $posts->withQueryString()->links('pagination.default') }}
        </div>
    </div>
</body>
