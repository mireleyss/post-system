<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::prefix('/categories')->group(Function() {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('categories.index');
    Route::get('/create', [CategoryController::class, 'create'])
        ->name('categories.create');
    Route::post('/create', [CategoryController::class, 'store'])
        ->name('categories.store');
    Route::get('/{category}/edit', [CategoryController::class, 'edit'])
        ->name('categories.edit');
    Route::put('/{category}', [CategoryController::class, 'update'])
        ->name('categories.update');
    Route::delete('/{category}', [CategoryController::class, 'destroy'])
        ->name('categories.destroy');
});

Route::prefix('/posts')->group(Function() {
    Route::get('/', [PostController::class, 'index'])
        ->name('posts.index');
    Route::get('/create', [PostController::class, 'create'])
        ->name('posts.create');
    Route::post('/create', [PostController::class, 'store'])
        ->name('posts.store');
    Route::get('/{post}/edit', [PostController::class, 'edit'])
        ->name('posts.edit');
    Route::put('/{post}', [PostController::class, 'update'])
        ->name('posts.update');
    Route::delete('/{post}', [PostController::class, 'destroy'])
        ->name('posts.destroy');
    Route::get('/filter', [PostController::class, 'filterByCategory'])
        ->name('posts.filter');
});
