<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'header' => 'required|string|min:3|max:64',
            'content' => 'required|string|min:10|max:10000',
            'category_id' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'header.required'       => 'A header is required',
            'header.min'            => 'A header is too small',
            'header.max'            => 'A header is too large',
            'content.required'      => 'A content is required',
            'content.min'           => 'A content is too small',
            'content.max'           => 'A content is too large',
            'category_id.required'  => 'Category is required',
        ];
    }
}
