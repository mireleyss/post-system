<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Services\CategoryService;
use App\Services\PostService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class PostController extends Controller
{
    public const POSTS_PER_PAGE = 5;
    private PostService $postService;
    private CategoryService $categoryService;

    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request, Category $category_id = null): View
    {
        $posts = $this->postService->query();

        if (!is_null($request['category_id'])) {
            $posts = $this->postService->filterByCategory($request['category_id']);
        }

        if (!is_null($category_id)) {
            $posts = $this->postService->filterByCategory($category_id);
        }

        $posts = $posts->paginate(PostController::POSTS_PER_PAGE);
        $categories = $this->categoryService->all();

        return view('posts.index', compact(['posts', 'categories']));
    }

    public function show(Post $post): View
    {
        return view('posts.show', compact($post));
    }

    public function create(): View
    {
        $categories = $this->categoryService->all();
        return view('posts.create', compact('categories'));
    }

    public function store(PostRequest $request): RedirectResponse
    {
        $this->postService->store($request->validated());

        return redirect()->route('posts.index');
    }

    public function edit(Post $post): View
    {
        $categories = $this->categoryService->all();

        return view('posts.edit', compact(['post', 'categories']));
    }

    public function update(PostRequest $request, Post $post): RedirectResponse
    {
        $this->postService->update($request->validated(), $post);

        return redirect()->route('posts.index');
    }

    public function destroy(Post $post): RedirectResponse
    {
        $this->postService->destroy($post);

        return redirect()->route('posts.index');
    }
}
