<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

class PostRepository
{
    public function all(): Collection
    {
        return Post::all();
    }

    public function paginate(int $items_on_page)
    {
        return Post::paginate($items_on_page);
    }

    public function query(): Builder
    {
        return Post::query();
    }

    public function filterByCategory($category)
    {
        return Post::where('category_id', $category);
    }

    public function store(array $data)
    {
        return Post::create($data);
    }

    public function update(array $data, Post $post): bool
    {
        return $post->update($data);
    }

    public function destroy(Post $post): ?bool
    {
        return $post->delete();
    }
}
