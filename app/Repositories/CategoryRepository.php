<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository
{
    public function all(): Collection
    {
        return Category::all();
    }

    public function store(array $data)
    {
        return Category::create($data);
    }

    public function update(array $data, Category $category): bool
    {
        return $category->update($data);
    }

    public function destroy(Category $category): ?bool
    {
        return $category->delete();
    }
}
