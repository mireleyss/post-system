<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

class PostService
{
    protected PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function all(): Collection
    {
        return $this->postRepository->all();
    }

    public function query(): Builder
    {
        return $this->postRepository->query();
    }

    public function filterByCategory($category)
    {
        return $this->postRepository->filterByCategory($category);
    }

    public function store(array $data)
    {
        return $this->postRepository->store($data);
    }

    public function update(array $data, Post $post): bool
    {
        return $this->postRepository->update($data, $post);
    }

    public function destroy(Post $post): ?bool
    {
        return $this->postRepository->destroy($post);
    }

    public function paginate(int $items_on_page)
    {
        return $this->postRepository->paginate($items_on_page);
    }

}
