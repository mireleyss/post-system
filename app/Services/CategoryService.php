<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Collection;

class CategoryService
{
    protected CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all(): Collection
    {
        return $this->categoryRepository->all();
    }

    public function store(array $data)
    {
        return $this->categoryRepository->store($data);
    }

    public function update(array $data, Category $category): bool
    {
        return $this->categoryRepository->update($data, $category);
    }

    public function destroy(Category $category): ?bool
    {
        return $this->categoryRepository->destroy($category);
    }

}
